import os
import label_image as lb
import logging

from flask import Flask, request, jsonify
from flask_cors import CORS

logging.basicConfig(level=logging.DEBUG)
UPLOAD_FOLDER = '/Users/greg/desktop/tmp/'

app = Flask(__name__)
CORS(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['JSON_SORT_KEYS'] = False


@app.route('/', methods = ['POST'])
def cat():
    file = request.files['file']

    file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))

    resp = jsonify(lb.run_script("retrained_graph.pb", "retrained_labels.txt", "Placeholder", "final_result", os.path.join(app.config['UPLOAD_FOLDER'] + file.filename)))
    resp.status_code = 200
    return resp
